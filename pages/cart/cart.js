// pages/cart/cart.js
Page({

  /**
   * 页面的初始数据
   */

  data: {
    toView: "n1",
    navList: [
      {
        id: "n1",
        nav: "红色",
        list: [
          { id: "1001", name: "红色名称1", pic: '../../images/homeBanner.jpg', count: 0, price: 88 },
          { id: "1002", name: "红色名称2", pic: '../../images/homeBanner.jpg', count: 0, price: 99 }
        ]
      },
      {
        id: "n2",
        nav: "绿色",
        list: [
          { id: "2001", name: "绿色名称1", pic: '../../images/homeBanner.jpg', count: 0, price: 45 },
          { id: "2002", name: "绿色名称2", pic: '../../images/homeBanner.jpg', count: 0 ,price:10}
        ]
      }, {
        id: "n3",
        nav: "红色",
        list: [
          { id: "3001", name: "名称1", pic: '../../images/homeBanner.jpg', count: 0,price:10 },
          { id: "3002", name: "名称2", pic: '../../images/homeBanner.jpg', count: 0,price:10 }
        ]
      }, {
        id: "n4",
        nav: "红色",
        list: [
          { id: 4001, name: "名称1", pic: '../../images/homeBanner.jpg', count: 0,price:10 },
          { id: 4002, name: "名称2", pic: '../../images/homeBanner.jpg', count: 0,price:10 }
        ]
      }, {
        id: "n5",
        nav: "红色",
        list: [
          { id: 5001, name: "名称1", pic: '../../images/homeBanner.jpg', count: 0 ,price:10},
          { id: 5002, name: "名称2", pic: '../../images/homeBanner.jpg', count: 0 ,price:10}
        ]
      }, {
        id: "n6",
        nav: "紫色",
        list: [
          { id: "6001", name: "紫色名称1", pic: '../../images/homeBanner.jpg', count: 0 ,price:10},
          { id: "6002", name: "紫色名称2", pic: '../../images/homeBanner.jpg', count: 0 ,price:10},
          { id: "6003", name: "紫色名称3", pic: '../../images/homeBanner.jpg', count: 0 ,price:10},
          { id: "6004", name: "紫色名称4", pic: '../../images/homeBanner.jpg', count: 0 ,price:10},
          { id: "6005", name: "紫色名称5", pic: '../../images/homeBanner.jpg', count: 0 ,price:10},
          { id: "6006", name: "紫色名称6", pic: '../../images/homeBanner.jpg', count: 0,price:10 }
        ]
      }

    ],
    currentTab: 0,
    cart: [],
    totalPrice: 0
  },
  //搜索框
  searchInput(e) {
    console.log(e.detail.value)
  },
  // 导航点击切换
  changeNavList(e) {
    this.setData({
      currentTab: e.currentTarget.dataset.index,
      toView: e.currentTarget.dataset.id
    })
  },
  // 页面上滑切换
  listViewScroll(e) {
    let scrollHeight = e.detail.scrollTop;
    let index = this.calculateIndex(this.data.heightArr, scrollHeight);
    this.setData({
      currentTab: index
    })

  },
  // 计算滚动的区间
  calculateHeight(height) {
    if (!this.data.heightArr.length) {
      this.data.heightArr.push(height)
    } else {
      let newH = this.data.heightArr[this.data.heightArr.length - 1] + height
      this.data.heightArr.push(newH);
    }
  },
  // 计算左边选中的下标
  calculateIndex(arr, scrollHeight) {
    let index = '';
    for (let i = 0; i < arr.length; i++) {
      if (scrollHeight >= 0 && scrollHeight < arr[0]) {
        index = 0;
      } else if (scrollHeight >= arr[i - 1] && scrollHeight < arr[i]) {
        index = i;
      }
    }
    return index;
  },

  // 查看详情
  openDetail(e) {
    const data = e.currentTarget.dataset.item
    console.log(data)
    wx.navigateTo({
      url: `/pages/detail/detail?id=${data.id}&name=${data.name}&count=${data.count}&pic=${data.pic}&price=${data.price}`
    })
  },
  // 加入购物车
  addCart(e) {
    const app = getApp()
    const item = e.target.dataset.item
    const cart = app.globalData.cart
    let num = 0
    cart.map(i=>{
      num += i.count
    })
    if(num >= 20){
      return false
    }
    if (cart.some(i => i.id == item.id)) {
      const index = cart.findIndex(i => i.id == item.id)
      cart[index].count++
    } else {
      item.count++
      cart.push(item)
    }
    
    this.setData({
      cart
    })
    this.getTotalPrice()
    this.getNewList()
  },
  reduceCart(e) {
    const app = getApp()
    const item = e.target.dataset.item
    const cart = app.globalData.cart
    const index = cart.findIndex(i => i.id == item.id)
    cart[index].count--
    if (cart[index].count == 0) {
      cart.splice(index, 1)
    }
    this.setData({
      cart
    })
    this.getTotalPrice()
    this.getNewList()
 
  },
  getNewList(){
    const app = getApp()
    let cart = app.globalData.cart
    let navList = this.data.navList
    navList.map(nav=>{
      nav.list.map(product=>{
        product.count = 0
        cart.map(i=>{
          if(i.id == product.id){
            product.count = i.count
          }
        })
      })
    })
   
this.setData({
  navList
})
  },
  getTotalPrice() {
    let totalPrice = 0
    const app = getApp()
    let cart = app.globalData.cart
    
    cart.map(item => {
      totalPrice += item.count * item.price
    })
    this.setData({ totalPrice })
  },
  // 选好了
  finishCart() {
wx.navigateTo({
  url: '/pages/show/show',
})
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      toView: 'a1',
      heightArr: []
    })
    let query = wx.createSelectorQuery();
    query.selectAll('.listView-type').boundingClientRect((rect) => {
      rect.forEach(ele => {
        this.calculateHeight(ele.height);
      })
    }).exec();
const app = getApp()
const cart = app.globalData.cart
this.setData({cart:cart})

      this.getTotalPrice()
      this.getNewList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})