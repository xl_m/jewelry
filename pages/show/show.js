import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';

// pages/show/show.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    number: 0,
    cart: [],
    totalPrice: 0,
    animationList: [],
    lineColor: ['gray'],
    //#region 纯数据字段
    /** 屏幕高度，单位px */
    _windowHeight: null,
    /** 屏幕高度，单位px */
    _windowWidth: null,
    /** 开始触摸时的单一项左上角y坐标 */
    _startOffsetY: null,
    /** 开始触摸时的单一项左上角x坐标 */
    _startOffsetX: null,
    /** 开始触摸位置y坐标 */
    _startPageY: null,
    /** 开始触摸位置x坐标 */
    _startPageX: null,
    /** 开始触摸项索引 */
    _startDragElementIndex: null,

    /** 滑动偏移 */
    _scrollThreshold: 0.5,

    /** 距顶部/左边多远时，触发 _scrollToUpper 事件，单位px，即上滑至屏幕顶部 */
    _upperThreshold: 100,
    /** 距底部/右边多远时，触发 _scrollToLower 事件，单位px，即下滑至屏幕底部 */
    _lowerThreshold: 100,
    /** 上滑和下滑时间，单位毫秒 */
    _scrollDuration: 1000,
    //#endregion

    /** 列表 */
    list: [],

    /** 单一项高度 */
    elementHeight: 40,

    /** 滑动项 */
    dragElement: null,
    /** movable-view组件y轴坐标，滑动时滑动项左上角距离文档顶部纵坐标，单位px */
    movableViewY: 0,
    /** movable-view组件y轴坐标，滑动时滑动项左上角距离文档顶部横坐标，单位px */
    movableViewX: 0,
    /** 滑动过程中经过的项 */
    lastTarget: null,
    /** 圆的坐标列 */
    pointData: null,
    /** x 差值 */
    dValueX: 0,
    /** y差值 */
    dValueY: 0,
    // 大圆半径  .box大小
    bcr: 300,
    // 珠子半径  .box view大小
    mcr: 40,
    // 里层小圆半径 .box::after 大小
    scr: 150,
    animationList: [],
  },
  // 前往购物车
  goToCart() {
    wx.navigateTo({
      url: '/pages/cart/cart',
    })
  },
  getTotalPrice() {
    let totalPrice = 0
    const app = getApp()
    let cart = app.globalData.cart

    cart.map(item => {
      totalPrice += item.count * item.price
    })
    this.setData({
      totalPrice
    })
  },
  finishDiy() {
    const list = this.data.list
    if (list.length < 10) {
      Dialog.alert({
        title: '温馨提示',
        message: '选珠不够，请继续添加'
      }).then(() => {
        // on close
      });
      return false
    }
    wx.navigateTo({
      url: '/pages/finish/finish',
    })
  },
  changeLineColor() {},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let num = (options.size || 4) * 2
    let elementHeight = 40
    if (num < 10) {
      num = 10
    } else if (num > 20) {
      num = 20
      elementHeight = 20
    }
    this.setData({
      number: num || 1,
      elementHeight,
    })
    const app = getApp()
    const cart = app.globalData.cart
    this.setData({
      cart: cart
    })
    this.getTotalPrice()
    this._getWindowHeightAndWidth();
  },
  /** 生成数据 */
  generateData(num) {
    let cart = this.data.cart
    let data = [];
    for (let i = 0; i < num; i++) {
      data.push({
        'id': i,
        animationData: {}
      })
    }
    this.setData({
      list: data
    })
  },
  /** 计算珠子的left */
  getComputedLeft(index) {
    let len = this.data.list.length;
    const {
      bcr,
      scr,
      mcr
    } = this.data;
    var a = Math.PI / 180
    //环形的半径=（大圆半径 - 小圆半径）/ 2 + 小圆半径；
    var r = (bcr - scr) / 2 + scr
    //根据span的数量来决定弧度大小
    var angle = 360 / len
    var left = (bcr + Math.sin(a * angle * index) * r) - mcr
    // console.log('index = ', index, 'left = ', left)
    // console.log('[index ' + index + ']   ' + 'left:' + left + 'rpx')
    return left + 'rpx'
    // let y = bcr + Math.cos(a * angle * i) * r;
  },
  /** 计算珠子的top */
  getComputedTop(index) {
    let len = this.data.list.length;
    const {
      bcr,
      scr,
      mcr
    } = this.data;
    var a = Math.PI / 180
    //环形的半径=（大圆半径 - 小圆半径）/ 2 + 小圆半径；
    var r = (bcr - scr) / 2 + scr
    //根据span的数量来决定弧度大小
    var angle = 360 / len
    // 倒序
    // var top = (bcr + Math.cos(a * angle * index) * r) - mcr
    var temp = (bcr + Math.cos(a * angle * index) * r)
    // 正序
    var top = bcr - Math.cos(a * angle * (len - index)) * r - mcr
    // console.log('[index ' + index + ']   ' + 'top:' + top + 'rpx')
    return top + 'rpx'
  },
  /** 获取页面上圆的坐标 */
  _getPoints() {
    let query = wx.createSelectorQuery().in(this);
    let that = this;
    query.selectAll('.item').boundingClientRect(function(rect) {
      let pointData = [];
      rect.forEach(item => {
        pointData.push([item.left, item.top])
      })
      that.setData({
        pointData
      })
    }).exec();
  },
  /** 长按触发事件 */
  onLongPress(event) {
    console.log('[onLongPress]', event);

    let dragElementIndex = event.currentTarget.dataset.index;
    let dragElement = this.data.list[dragElementIndex];
    let dragElementPoint = this.data.pointData[dragElementIndex];
    this.setData({
      dValueX: dragElementPoint[0] - event.target.offsetLeft,
      dValueY: dragElementPoint[1] - event.target.offsetTop
    })
    console.log('[elPoint]: ', dragElementPoint)
    console.log('[currentPoint]: ', [event.target.offsetLeft, event.target.offsetTop])
    this.setData({
      /** 点击项左上角y坐标 */
      _startOffsetY: event.target.offsetTop,
      /** 点击项左上角x坐标 */
      _startOffsetX: event.target.offsetLeft,
      /** 点击位置y坐标 */
      _startPageY: event.touches[0].pageY,
      /** 点击位置x坐标 */
      _startPageX: event.touches[0].pageX,
      /** 点击项索引 */
      _startDragElementIndex: dragElementIndex,
      /** 点击项 */
      dragElement,
      /** movable-view组件左上角y坐标 */
      movableViewY: event.target.offsetTop,
      /** movable-view组件左上角y坐标 */
      movableViewX: event.target.offsetLeft
    });
  },
  /**
   * 手指触摸后移动
   * - 触底或触顶时下滑或者上滑
   * - 移动movable-view
   */
  onTouchMove(event) {
    console.log('[onTouchMove]', event);

    // 长按事件
    if (this.data.dragElement) {
      /** 触摸点位置在显示屏幕区域左上角Y坐标 */
      let clientY = event.touches[0].clientY;
      /** 触摸点位置在显示屏幕区域左上角X坐标 */
      let clientX = event.touches[0].clientX;
      /** 触摸点位置距离文档左上角X坐标 */
      let pageX = event.touches[0].pageX;
      /** 触摸点位置距离文档左上角Y坐标 */
      let pageY = event.touches[0].pageY;
      /** 和最初点击位置比较移动距离 */
      let targetMoveDistance = {};
      targetMoveDistance.Y = pageY - this.data._startPageY;
      targetMoveDistance.X = pageX - this.data._startPageX;
      /** 移动后的movable-view组件位置 */
      let movableViewY = this.data._startOffsetY + targetMoveDistance.Y;
      /** 移动后的movable-view组件位置X */
      let movableViewX = this.data._startOffsetX + targetMoveDistance.X;
      /** 经过项索引 */
      let targetIndex = this._computeFutureIndex(targetMoveDistance, this.data._startDragElementIndex);
      if (targetIndex === false) {
        targetIndex = null;
      }

      this.setData({
        movableViewX,
        movableViewY,
        lastTarget: targetIndex
      });
    }
  },
  /** 滑动结束 */
  onTouchEnd(event) {
    console.log('[onTouchEnd]', event);

    if (this.data.dragElement) {
      let list = this._deepCopy(this.data.list);
      /** 结束点位置y坐标 */
      let pageY = event.changedTouches[0].pageY;
      /** 结束点位置x坐标 */
      let pageX = event.changedTouches[0].pageX;
      /** 和初始点击位置比较移动距离 */
      let targetMoveDistance = {}
      targetMoveDistance.Y = pageY - this.data._startPageY;
      targetMoveDistance.X = pageY - this.data._startPageX;
      /** 初始点击项索引 */
      let dragElementIndex = this.data._startDragElementIndex;
      /** 目标项索引 */
      // const futureIndex = this._computeFutureIndex(targetMoveDistance, dragElementIndex);
      if (this.data.lastTarget) {
        list[dragElementIndex] = list.splice(this.data.lastTarget, 1, list[dragElementIndex])[0]; // 移动位置
        // arr[index1] = arr.splice(index2, 1, arr[index1])[0];
        console.log(list)
      }

      this.setData({
        list,
        dragElement: null,
        lastTarget: null
      });
    }
  },
  /** 阻止滑动 */
  onHackTouchMove() {},
  /** 获取可使用窗口高度，单位px */
  _getWindowHeightAndWidth() {
    try {
      const {
        windowHeight,
        windowWidth
      } = wx.getSystemInfoSync();
      this.setData({
        _windowHeight: windowHeight,
        _windowWidth: windowWidth
      });
    } catch (err) {
      console.error('[_getWindowHeight]', err);
    }
  },
  /** 页面滑动 */
  _pageScroll(clientY, pageY) {
    if (clientY + this.data._upperThreshold >= this.data._windowHeight) {
      // 下滑接近屏幕底部
      wx.pageScrollTo({
        scrollTop: pageY + this.data.elementHeight,
        duration: this.data._scrollDuration
      });
    } else if (clientY - this.data._lowerThreshold <= 0) {
      // 上滑接近屏幕顶部
      wx.pageScrollTo({
        scrollTop: pageY - this.data.elementHeight,
        duration: this.data._scrollDuration
      })
    }
  },
  /**
   * 计算目标索引
   * @param {number} targetMoveDistance 移动距离
   * @param {number} dtagElementIndex 初始移动项索引
   * 若轻轻拂动则返回false
   */
  _computeFutureIndex(targetMoveDistance, dragElementIndex) {
    let arr = [];
    let data = this.data.pointData;
    let point = [data[dragElementIndex][0] + targetMoveDistance.X, data[dragElementIndex][1] + targetMoveDistance.Y];
    for (let i = 0; i < data.length; i++) {
      arr.push(Math.hypot(data[i][0] - point[0], data[i][1] - point[1]))
    }
    let min = Math.min.apply(null, arr);
    console.log('[moveTo]: ', arr.indexOf(min))
    if (min < 30) {
      return arr.indexOf(min)
    } else {
      return false
    }
    // return arr.indexOf(min)
  },
  /** 深拷贝 */
  _deepCopy(obj) {
    // 只拷贝对象
    if (typeof obj !== 'object') return;
    // 根据obj的类型判断是新建一个数组还是一个对象
    var newObj = obj instanceof Array ? [] : {};
    for (var key in obj) {
      // 遍历obj,并且判断是obj的属性才拷贝
      if (obj.hasOwnProperty(key)) {
        // 判断属性值的类型，如果是对象递归调用深拷贝
        newObj[key] = typeof obj[key] === 'object' ? this._deepCopy(obj[key]) : obj[key];
      }
    }
    return newObj;
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // const cart = this.data.cart
    // let num = 0
    // cart.map(i=>{
    //   num += i.count
    // })
    this.generateData(15);
    // const list = this.data.list
    // for(let i = 0;i<list.length; i++){
    //   let animation = wx.createAnimation({
    //     duration: 1000,
    //     timingFunction: 'ease',
    //   })
    //   this.data.list[i].animationData = animation
    //   this.data.list[i].animationData.translate(0,0).step()   
    //   this.data.list[i].animationData = animation.export()
    // }
    this.setData({
      list: this.data.list
    })
    setTimeout(this.translateTopAndLeft, 500)

  },

  translateTopAndLeft: function() {
    const list = this.data.list
    let pointArr = [];
    for (let i = 0; i < list.length; i++) {
      // this.data.list[i].animationData = animation
      let top = this.getComputedTop(i)
      let left = this.getComputedLeft(i)
      pointArr.push([top, left]);

    }
    let animationList = [...this.data.animationList];
    for (let i = 0; i < pointArr.length; i++) {
      let animation = wx.createAnimation({
        duration: 1000,
        timingFunction: 'ease',
      })
      let currentArrary = pointArr.slice(0, i + 1);
      currentArrary.map((item) => {
        let [top, left] = item;
        animation.top(top).left(left).step({
          duration: 400
        });
      })
      // animation.step({ duration: 500 })
      // console.log(animation.export())
      animationList.push(animation.export());
      // for (let j = 0; j <= i; j++) {
      //   let [top, left] = pointArr[j];
      //   animation.top(top).left(left).step({duration: 500})
      //   // this.data.list[i].animationData = animation.export()
      // }
      // this.data.list[i].animationData = animation.export()
    }

    this.setData({
      list: this.data.list,
      animationList
    })
    setTimeout(function() {
      console.log('finsh')
      this._getPoints(); //官方写法就这样.暂时没有找到相关api.
    }.bind(this), 5000)


  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})